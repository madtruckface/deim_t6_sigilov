using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class GhostMovement : MonoBehaviour
{
    public NavMeshAgent navMeshAgent;
    public Vector3 destinationPoint;
    public Transform destinationObject;

    public List<Transform> destinationObjects;

    private int currentDestinationIndex;

    // Start is called before the first frame update
    void Start()
    {
        GameObject initialPoint = new GameObject("Initial Point");
        initialPoint.transform.position = transform.position;
        destinationObjects.Insert(0, initialPoint.transform);
        navMeshAgent.SetDestination(destinationObjects[0].position);
        currentDestinationIndex = 0;
    }

    // Update is called once per frame
    void Update()
    {
        //if (transform.position == destinationObjects[currentDestinationIndex].position)
        if (navMeshAgent.remainingDistance <= 0.1f)
        {
            Debug.Log("ola");
            currentDestinationIndex += 1;
            if (currentDestinationIndex >= destinationObjects.Count)
            {
                currentDestinationIndex = 0;
                Debug.Log("index");
            }
            navMeshAgent.SetDestination(destinationObjects[currentDestinationIndex].position);
        }
    }
}
