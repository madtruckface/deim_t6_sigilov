using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationGargyle : MonoBehaviour
{
    public float velocity;
    public float rotate;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.localEulerAngles = new Vector3(0, Mathf.PingPong(Time.time * velocity, rotate));
    }
}
